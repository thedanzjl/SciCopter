import sys
import os
import json
import datetime
import serial
import time
import urllib.request
from serial.tools import list_ports
from PyQt5 import Qt
from PyQt5 import uic
import pyqtgraph as pg


class GraphWidget(Qt.QWidget):

    measures = {'time': 'sec', 'humidity': '%', "height": 'm', 'temperature': 'C'}
    VG_PRECISION = 3

    def __init__(self, x, y, x_name, y_name, graph_info=False, add_data='', online_mode=False, parent=None):
        super().__init__(parent=parent)
        self.order_data = Data.order_data
        self.changeable_data = Data.changeable_data
        self.layout = Qt.QVBoxLayout(self)
        self.x, self.y = x, y
        self.x_name = x_name
        self.y_name = y_name
        self.parent = parent
        self.graph_info = graph_info
        self.add_data = add_data
        if online_mode:
            self.online()
        self.init_ui(x, y)
        self.signals()

    def get_vg(self, parametr):
        """Get vertical gradient"""
        result = []
        for i in range(0, len(parametr), 100):
            t = parametr[i:i+101]
            if len(t) < 90: 
                break
            f, l = t[:3], t[-3:]
            grad = list(map(lambda x: x[1]-x[0], list(zip(f, l))))
            aver_grad = sum(grad)/len(grad)
            print(i, f, l, aver_grad)
            result.append(aver_grad)
        print(result)
        return round(sum(result)/len(result), self.VG_PRECISION)

    def signals(self):
        self.combo_box.currentTextChanged.connect(self.switch_graph)

    def switch_graph(self, text):
        if text == 'general':
            self.curve.setData(self.x, self.y)
            self.plot.plotItem.setLabel("bottom", self.x_name)
            self.plot.plotItem.setLabel("left", self.y_name)
        elif text == 'derivative':
            xbigger = self.x[::-1][1:]
            xsmaller = self.x[::-1][:-1]
            delta_x = [b-s for b, s in list(zip(xbigger, xsmaller))]
            self.curve.setData(self.y[1:], delta_x)
            self.plot.plotItem.setLabel("bottom", self.y_name)
            self.plot.plotItem.setLabel("left", "<font>&Delta;</font>"+self.x_name)

    def init_ui(self, x, y):
        self.plot = pg.PlotWidget()
        self.curve = self.plot.plot()
        self.curve.setData(x, y)
        self.plot.plotItem.setLabel("bottom", self.x_name+', '+self.measures.get(self.x_name, ''))
        self.plot.plotItem.setLabel("left", self.y_name+', '+self.measures.get(self.y_name, ''))
        if self.graph_info:
            beauty = lambda x: int(x) if x//1 == x else round(x, 1)
            info = '{}: {}/{}/{}; {}: {}/{}/{}; ' \
                   ''.format(self.x_name, beauty(min(x)), beauty(sum(x)/len(x)), beauty(max(x)),
                             self.y_name, beauty(min(y)), beauty(sum(y)/len(y)), beauty(max(y)))
            if self.x_name in self.changeable_data:
                info += 'vg = {}'.format(self.get_vg(self.x))
            label = Qt.QLabel()
            font = label.font()
            font.setPointSize(21)
            label.setFont(font)
            label.setText(info)
            label.setToolTip('graph info: min/middle/max')
            self.layout.addWidget(label)
        self.layout.addWidget(self.plot)
        self.combo_box = Qt.QComboBox()
        self.combo_box.addItems(['general', 'derivative'])
        grid = Qt.QGridLayout()
        if self.add_data:
            label = Qt.QLabel()
            label.setText(self.add_data)
            grid.addWidget(label, 0, 0)
        grid.setColumnStretch(0, 1)
        grid.addWidget(self.combo_box, 0, 2)
        self.layout.addLayout(grid)

    def online(self):
        self.curves = {}
        items = list(self.measures.keys())
        del items[items.index('time')]
        grid = Qt.QGridLayout()
        self.boxes = {item: Qt.QCheckBox() for item in items}
        i = 0
        for item in items:
            grid.addWidget(Qt.QLabel(item), 0, i)
            i += 1
            grid.addWidget(self.boxes[item], 0, i)
            i += 1
        self.layout.addLayout(grid)

    def updateGraph(self, time, **kwargs):
        """ kwargs example: temperature=[...], humidity=[...], height=[...] """
        for key in kwargs:
            if self.boxes[key].isChecked():
                if key not in self.curves:
                    self.curves[key] = self.plot.plot()
                    # self.curves[key].setPen(Qt.QPen(Qt.QColor(127, 127, 127)))
                self.curves[key].setData(time, kwargs[key])
            else:
                if key in self.curves:
                    self.curves[key].clear()
                    del self.curves[key]
        Qt.QApplication.processEvents()


class SuperCalendarWidget(Qt.QCalendarWidget):

    """ Format of date_available argument: ['day/month/year', ...] """

    da = None
    clean = False

    def __init__(self, date_available, parent=None):
        super().__init__(parent)
        self.g_color = Qt.QColor(Qt.Qt.green)
        self.w_color = Qt.QColor(Qt.Qt.white)
        self.g_color.setAlpha(64)
        self.w_color.setAlpha(64)
        self.chooseDate(date_available)

    def paintCell(self, painter, rect, date):
        super().paintCell(painter, rect, date)
        if not self.clean:
            if date in self.da:
                painter.fillRect(rect, self.g_color)
        else:
            painter.fillRect(rect, self.w_color)
            self.clean = False

    def chooseDate(self, date_available):
        if self.da is not None:
            self.clear()
        self.da = [Qt.QDate(*list(map(int, item.split('/')[::-1]))) for item in date_available]
        self.clean = False
        self.updateCells()

    def clear(self):
        if self.da is None:
            self.clean = True
            self.updateCells()



class Data:

    data = list()
    order_data = ['lat', 'lng', 'date', 'time', 'day of week', 'temperature', 'humidity', 'height']
    changeable_data = ['temperature', 'humidity', 'height']

    def __init__(self, raw_data):
        self.raw_data = raw_data

    def init_data(self):
        for raw_line in self.raw_data:
            line = {}
            for i, item in enumerate(raw_line.split()):
                line[self.order_data[i]] = item
            self.data.append(line)
        self.coordinates_to_place()

    def coordinates_to_place(self):
        result = dict()
        raw_coordinates = [[dict_.get('lng'), dict_.get('lat')] for dict_ in self.data]
        round_coord = lambda x: str(round(float(x), 2))
        coordinates = set(map(lambda x: tuple(map(round_coord, x)), raw_coordinates))
        for coordinate in coordinates:
            try:
                place = urllib.request.urlopen('https://geocode-maps.yandex.ru/'
                                               '1.x/?geocode={}, {};format=json;lang=en_US;results=1'.
                                               format(*coordinate), timeout=2)
                place = json.loads(place.read().decode())
                place = place['response']['GeoObjectCollection']['featureMember'][0]\
                    ['GeoObject']['metaDataProperty']['GeocoderMetaData']['text']
                country, city = place.split(", ")[:2]
                result[coordinate] = "%s, %s" % (country, city)
            except urllib.request.URLError:
                result[coordinate] = ', '.join(coordinate)
            except KeyError:
                result[coordinate] = ', '.join(coordinate)
        for i, dict_ in enumerate(self.data):
            self.data[i]['place'] = result.get((round_coord(dict_.get('lng')),
                                                round_coord(dict_.get('lat'))), "sadf")
            del self.data[i]['lng'], self.data[i]['lat']

    def get_time_from_date(self, date):
        return [dict_['time'] for dict_ in self.data if dict_['date'] == date]

    def get_day_of_week(self, place, date):
        for d in self.data:
            if d.get('place') == place and d.get('date') == date:
                return d.get('day of week')

    def get_dependence(self, item1, item2, place, date):
        '''format: ([0, 2, 3, 5], [23, 25, 22, 28])'''
        result1, result2 = [], []
        for dict_ in self.data:
            if dict_.get('date') == date and dict_.get('place') == place:
                if item1 != 'time':
                    result1.append(dict_[item1])
                result2.append(dict_[item2])
        if item1 == 'time':
            result1 = list(map(self.to_seconds, self.get_time_from_date(date)))
            result1 = list(map(lambda x: x-min(result1), result1))
        result1 = list(map(float, result1))
        result2 = list(map(float, result2))
        return result1, result2

    @staticmethod
    def to_seconds(str_time):
        ints = list(map(int, str_time.split(':')))
        return ints[0]*3600 + ints[1]*60 + ints[2]

    def min_max_time(self, date):
        times = {self.to_seconds(item): item for item in self.get_time_from_date(date)}
        return times[min(list(times.keys()))], times[max(list(times.keys()))]

    def validate(self):
        try:
            for line in self.raw_data:
                c, n = line.split(), self.order_data
                assert len(c) == len(n)
        except AssertionError as e:
            print(str(e))
            return False
        return True

    def date_available(self, place):
        return list(set([item.get('date') for item in self.data if item.get('place') == place]))

    def all_of_date(self):
        return list(set([item.get('date') for item in self.data]))

    def year_available(self, place, year):
        return list(filter(lambda x: x.endswith(str(year)), self.date_available(place)))

    def month_available(self, place, year, month):
        return list(filter(lambda x: x[:x.rfind('/')].endswith(str(month)), self.year_available(place, str(year))))

    def places_available(self):
        return list(set([item.get('place') for item in self.data]))

    def update(self, raw_line):
        line = {}
        for i, item in enumerate(raw_line.split()):
            line[self.order_data[i]] = item
        self.data.append(line)

    def date_range(self, place):
        total, result = [], []
        da = self.date_available(place)
        if len(da) == 1: 
            return da
        da = [datetime.datetime(*list(map(int, date.split("/")[::-1]))) for date in da]
        da.sort()
        for i, date in enumerate(da[:-1]):
            if (da[i+1]-da[i]).days == 1:
                sign = '-'
            else:
                sign = ','
            if sign == ',' or i == 0:
                result.append(da[i])
                result.append(sign)
        result.append(da[-1])
        for i, item in enumerate(result):
            if not isinstance(item, str):
                result[i] = '{}/{}/{}'.format(item.day, item.month, item.year)
        for i, item in enumerate(result):
            if item == "-":
                total.append((result[i-1], result[i+1]))
            if item == ',':
                total.append(result[i+1])
        return total


def init_proxy(host='192.168.105.50', port=3128):
    try:
        proxy = urllib.request.ProxyHandler({'https': "{}:{}".format(host, str(port))})
        opener = urllib.request.build_opener(proxy)
        urllib.request.install_opener(opener)
    except Exception as e:
        print(str(e))



class SciQuadcopterApp(Qt.QMainWindow):

    data = None
    ui = None
    port = None
    dmaker = None
    s_port = None
    file_dict = {}

    def __init__(self):
        super().__init__()
        self.dependencies = Data.changeable_data
        self.init_ui()
        self.init_menu()
        self.load_config()

    def remove_tab(self, index):
        self.main_widget.removeTab(index)

    def load_config(self):
        path = os.path.expanduser("~") + '/.quadcopter.cfg'
        try: 
            with open(path, 'r') as file:
                self.file_dict = json.load(file)
        except FileNotFoundError:
            pass
        if self.file_dict:
            self.open_file(path=path)

    def closeEvent(self, *args):
        '''save cfg file'''
        path = os.path.expanduser("~") + '/.quadcopter.cfg'
        with open(path, 'w') as file:
            json.dump(self.file_dict, file)
        if self.ui is not None:
            self.ui.close()

    def init_ui(self):
        self.move(300, 150)
        self.resize(700, 700)
        self.setWindowTitle("SciQuadcopter")
        self.setWindowIcon(Qt.QIcon('SQicon.png'))
        self.main_widget = Qt.QTabWidget(self)
        self.main_widget.tabCloseRequested.connect(self.remove_tab)
        self.setCentralWidget(self.main_widget)
        self.status_label = Qt.QLabel()
        self.statusBar().addPermanentWidget(self.status_label)
        self.main_widget.setTabsClosable(True)
        self.main_widget.setMovable(True)

    def init_menu(self):
        # filemenu
        openAction = Qt.QAction(Qt.QIcon('openicon.png'), 'Open...', self)
        openAction.setShortcut("Ctrl+O")
        openAction.triggered.connect(self.open_file)
        closeAction = Qt.QAction(Qt.QIcon(), 'Exit', self)
        closeAction.setShortcut("Ctrl+Q")
        closeAction.triggered.connect(self.close)
        # onlinemenu
        portAction = Qt.QAction("port", self)
        portMenu = Qt.QMenu()
        ports = [port.device for port in list_ports.comports()]
        actionports = []
        for port in ports:
            action = Qt.QAction(port, self)
            action.triggered.connect(self.selected_port)
            actionports.append(action)
        if len(actionports) == 1:
            self.port = actionports[0].text()
        elif len(actionports) == 0:
            self.port = ''
        # viewmenu
        self.dmakerAction = Qt.QAction('Dependence maker', self)
        self.dmakerAction.setCheckable(True)
        self.dmakerAction.setChecked(True)
        #
        portMenu.addActions(actionports)
        portAction.setMenu(portMenu)
        menu = self.menuBar()
        filemenu = menu.addMenu("File")
        filemenu.addAction(openAction)
        filemenu.addAction(closeAction)
        onlinemenu = menu.addMenu("Online")
        onlinemenu.addAction(portAction)
        onlinemenu.addAction('begin', self.connect)
        viewmenu = menu.addMenu('View')
        viewmenu.addAction(self.dmakerAction)

    def selected_port(self):
        self.port = self.sender().text()

    def connect(self):
        if self.port is None:
            self.error('Port is not selected')
            return
        elif not self.port:
            self.error('No available ports')
            return
        self.dmaker.setVisible(False)
        self.dmakerAction.setChecked(False)
        self.s_port = serial.Serial(self.port, baudrate='9600')
        time.sleep(2)
        self.online_graph = GraphWidget([0], [0], 'time', '', online_mode=True)
        self.main_widget.addTab(self.online_graph, "Online")
        self.main_widget.setCurrentWidget(self.online_graph)
        self.online_data = Data([])
        self.on_line_update = Qt.QTimer()
        self.on_line_update.timeout.connect(self.on_line)
        self.on_line_update.start(500)

    def on_line(self):
        raw_data = self.s_port.readline()
        self.online_data.update(raw_data)
        place = raw_data.split()[Data.order_data.index('place')]
        date = raw_data.split()[Data.order_data.index('date')]
        temp = self.online_data.get_dependence('time', 'temperature', place, date)
        hum = self.online_data.get_dependence('time', 'humidity', place, date)
        height = self.online_data.get_dependence('time', 'height', place, date)
        self.online_graph.updateGraph(hum[0], temperature=temp[1], humidity=hum[1], height=height[1])

    def date_changed(self):
        place = self.ui.placeBox.currentText()
        if isinstance(self.sender(), SuperCalendarWidget):
            qdate = self.ui.calendarWidget.selectedDate()
            self.ui.date_edit.setDate(qdate)
        else:
            qdate = self.ui.date_edit.date()
            self.ui.calendarWidget.setSelectedDate(qdate)
        day = '{:02d}/{:02d}/{}'.format(qdate.day(), qdate.month(), qdate.year())
        day = 'available' if day in self.data.date_available(place) else 'not available'
        self.ui.date_label.setText('year({}) month({}) day({})\n\n'
                                   .format(len(self.data.year_available(place, qdate.year())),
                                           len(self.data.month_available(place, qdate.year(), qdate.month())),
                                           day))

    def place_changed(self):
        self.ui.plainTextEdit.clear()
        place = self.ui.placeBox.currentText()
        for item in self.data.date_range(place):
            if isinstance(item, tuple):
                insert_word = ' - '.join(item)
            else:
                insert_word = item
            self.ui.plainTextEdit.insertPlainText(insert_word + '\n')
        self.ui.calendarWidget.clear()
        self.ui.calendarWidget.chooseDate(self.data.date_available(place))
        self.file_dict['place'] = self.ui.placeBox.currentText()

    def open_dependence_maker(self):
        self.ui = uic.loadUi('dependence_maker.ui')
        self.ui.x_box.addItems(self.dependencies)
        self.ui.x_box.addItem('time')
        self.ui.y_box.addItems(self.dependencies[::-1])
        # place
        p_a = self.data.places_available()
        print(p_a)
        self.ui.placeBox.addItems(p_a)
        self.ui.placeBox.activated.connect(self.place_changed)
        place = p_a[0]
        # date
        date = Qt.QDate(*list(map(int, self.file_dict.get('date', self.data.date_available(place)[0]).split("/")[::-1])))
        self.ui.date_edit.setDate(date)
        self.ui.date_edit.dateChanged.connect(self.date_changed)
        ##
        self.ui.calendarWidget = SuperCalendarWidget(self.data.date_available(place))
        self.ui.calendarLayout.addWidget(self.ui.calendarWidget)
        self.ui.calendarWidget.setSelectedDate(date)
        self.date_changed()
        self.ui.calendarWidget.selectionChanged.connect(self.date_changed)
        self.place_changed()
        ###
        self.ui.makeButton.clicked.connect(self.init_dependence)
        self.dmaker = Qt.QDockWidget(self)
        self.dmaker.setMinimumWidth(0)
        self.dmaker.setWidget(self.ui)
        self.dmaker.setFeatures(Qt.QDockWidget.DockWidgetMovable | Qt.QDockWidget.DockWidgetFloatable)
        self.addDockWidget(Qt.Qt.LeftDockWidgetArea, self.dmaker)

    def init_dependence(self):
        place = self.ui.placeBox.currentText()
        qdate = self.ui.date_edit.date()
        date = '{:02d}/{:02d}/{}'.format(qdate.day(), qdate.month(), qdate.year())
        if date not in self.data.date_available(place):
            self.error('This day is not available')
            return
        self.file_dict['date'] = date
        item1 = self.ui.x_box.currentText()
        item2 = self.ui.y_box.currentText()
        try:
            dep = self.data.get_dependence(item1, item2, place, date)
            if set(dep[0]) == {'nan'}:
                raise Exception("No data available in this day: " + item1)
            if set(dep[1]) == {'nan'}:
                raise Exception("No data available in this day: " + item2)
        except Exception as e:
            self.error(e)
            return
        graph_widget = GraphWidget(
            *dep, item1, item2, graph_info=True,
            add_data='{};   {};   ({} - {})'.format(
                place, date, *self.data.min_max_time(date)),
            parent=self)
        self.main_widget.addTab(graph_widget, '{}:{}'.format(item1, item2))
        self.main_widget.setCurrentWidget(graph_widget)

    def error(self, e):
        Qt.QMessageBox.critical(self, "Error", str(e))

    def open_file(self, path=''):
        if not path:
            directory = self.file_dict.get('path') if self.file_dict else ''
            filename = Qt.QFileDialog.getOpenFileName(self, "Open File", directory=directory)[0]
        else:
            filename = self.file_dict['path']
        try:
            with open(filename, 'r') as file:
                self.file_dict['path'] = filename
                self.data = Data(file.readlines())
                if not self.data.validate():
                    if not path:
                        self.error("Invalid file")
                    return
                self.data.init_data()
            self.status_label.setText(filename)
            if self.dmaker is None:
                self.open_dependence_maker()
                self.dmakerAction.triggered.connect(self.dmaker.setVisible)
        except FileNotFoundError:
            self.file_dict['path'] = ''


def main():
    app = Qt.QApplication([])
    if sys.argv[1:]:
        init_proxy(*sys.argv[1].split(":"))
    aw = SciQuadcopterApp()
    aw.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
